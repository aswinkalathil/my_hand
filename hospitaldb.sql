-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: localhost    Database: hospital
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Doctor`
--

DROP TABLE IF EXISTS `Doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Doctor` (
  `Name` text,
  `Did` int(11) NOT NULL DEFAULT '0',
  `dpassword` varchar(20) DEFAULT NULL,
  `category` text NOT NULL,
  `timing` text,
  `image` text,
  PRIMARY KEY (`Did`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Doctor`
--

LOCK TABLES `Doctor` WRITE;
/*!40000 ALTER TABLE `Doctor` DISABLE KEYS */;
INSERT INTO `Doctor` VALUES ('manu',1,'1','',NULL,NULL),('Dr.shyamprasad',10001,'1234','Surgen','10:00:00','\"/pictures/folder/png/Surgen dp.png\"'),('Dr.supriya',10002,'1235','Pediatrician','16:00:00','\"/pictures/folder/png/Pediatritian dp.png\"'),('Dr.rajagopal',10003,'1236','General','17:00:00','\"/pictures/folder/png/General Dr dp.png\"');
/*!40000 ALTER TABLE `Doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Patient`
--

DROP TABLE IF EXISTS `Patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Patient` (
  `Name` text,
  `opno` int(11) NOT NULL,
  `Address` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `insno` text,
  `phone` text,
  `gender` text NOT NULL,
  PRIMARY KEY (`opno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Patient`
--

LOCK TABLES `Patient` WRITE;
/*!40000 ALTER TABLE `Patient` DISABLE KEYS */;
INSERT INTO `Patient` VALUES ('Aswin',10000,'kalathil','2020-03-24','56','23','male'),('achu',10001,'','2000-09-09','','','female'),('dundu',10002,'','1990-01-01','','','female');
/*!40000 ALTER TABLE `Patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appointment`
--

DROP TABLE IF EXISTS `appointment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appointment` (
  `opno` text NOT NULL,
  `did` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `app_time` time DEFAULT '00:00:00',
  UNIQUE KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointment`
--

LOCK TABLES `appointment` WRITE;
/*!40000 ALTER TABLE `appointment` DISABLE KEYS */;
INSERT INTO `appointment` VALUES ('10000','10003','2020-04-29 06:27:53','00:00:00'),('10000','10001','2020-04-29 06:29:32','00:00:00'),('null','10002','2020-04-29 06:29:47','00:00:00'),('10000','10002','2020-04-29 08:27:40','00:00:00'),('10000','10003','2020-04-30 11:40:49','00:00:00');
/*!40000 ALTER TABLE `appointment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient_record10001`
--

DROP TABLE IF EXISTS `patient_record10001`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_record10001` (
  `slno` text,
  `complaint` text,
  `dignosis` text,
  `treatment` text,
  `did` text,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient_record10001`
--

LOCK TABLES `patient_record10001` WRITE;
/*!40000 ALTER TABLE `patient_record10001` DISABLE KEYS */;
INSERT INTO `patient_record10001` VALUES ('0',NULL,NULL,NULL,NULL,'2020-03-24 12:47:01'),('1','pani1','','','DID','2020-03-24 12:49:50'),('2','2','','','null','2020-03-24 12:52:21'),('3','3','','','null','2020-03-24 12:56:51');
/*!40000 ALTER TABLE `patient_record10001` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient_record10002`
--

DROP TABLE IF EXISTS `patient_record10002`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_record10002` (
  `slno` text,
  `complaint` text,
  `dignosis` text,
  `treatment` text,
  `did` text,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient_record10002`
--

LOCK TABLES `patient_record10002` WRITE;
/*!40000 ALTER TABLE `patient_record10002` DISABLE KEYS */;
INSERT INTO `patient_record10002` VALUES ('0',NULL,NULL,NULL,NULL,'2020-03-30 07:41:53'),('1','1','','','null','2020-03-30 07:42:31');
/*!40000 ALTER TABLE `patient_record10002` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-12 14:25:56
